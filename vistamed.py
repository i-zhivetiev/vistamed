# coding=utf8

import os
import sys
from ConfigParser import RawConfigParser

import MySQLdb as mysql
from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_MainWindow(object):
    def __init__(self, **kwargs):
        try:
            self.connection = mysql.connect(**kwargs)
        except:
            msg = "Couldn't connect to the server."
            raise RuntimeError(msg)

        self.fields = [
            'Фамиля',
            'Имя',
            'Отчество',
            'Дата рождения',
            'Возраст',
            'Пол',
            'ОМС Серия',
            'ОМС №',
            'Документ',
            'Серия',
            'Номер',
        ]

        self.load_in_progress = False

    def __del__(self):
        self.connection.close()

    def get_sex(self, code):
        sex = {
            0: 'Неизвестно',
            1: 'М',
            2: 'Ж',
        }

        return sex[code]

    def refreshTable(self):
        self.loadData(_query)

    def loadData(self, query, *args):

        self.load_in_progress = True
        cursor = self.connection.cursor()

        row_number = cursor.execute(query, args)
        self.tableWidget.setRowCount(row_number)

        for row_number, row_data in enumerate(cursor):
            row_data = list(row_data)
            row_data[5] = self.get_sex(row_data[5])

            for col_number, cell_data in enumerate(row_data):
                self.tableWidget.setItem(
                    row_number,
                    col_number,
                    QtGui.QTableWidgetItem(_fromUtf8(str(cell_data)))
                )

        cursor.close()

        self.tableWidget.resizeColumnsToContents()
        self.tableWidget.resizeRowsToContents()

        self.load_in_progress = False

    def cellChandged(self, row, col):
        if self.load_in_progress:
            return

        item = self.tableWidget.item(row, col)
        contents = item.text()

        # после редактирования
        # - сформировать запрос
        # - вызвать loadData

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("Виста"))
        MainWindow.resize(940, 368)

        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))

        self.pushButton = QtGui.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(830, 320, 100, 30))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.refreshTable)

        self.tableWidget = QtGui.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(20, 20, 905, 291))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(11)
        self.tableWidget.setRowCount(1)
        self.tableWidget.setHorizontalHeaderLabels(
            [_fromUtf8(s) for s in self.fields]
        )

        self.tableWidget.cellChanged.connect(self.cellChandged)

        MainWindow.setCentralWidget(self.centralwidget)

        self.refreshTable()

        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Виста", None))
        self.pushButton.setText(_translate("MainWindow", "Обновить", None))


if __name__ == "__main__":

    _query = '''\
SELECT
  Client.lastName, Client.firstName, Client.patrName,
  Client.birthDate, age(Client.birthDate, curdate()) AS age,
  Client.sex,
  ClientPolicy.serial,
  ClientPolicy.number,
  rbDocumentType.code,
  ClientDocument.serial,
  ClientDocument.number
FROM Client
LEFT JOIN ClientPolicy ON Client.id = ClientPolicy.client_id
LEFT JOIN ClientDocument ON Client.id = ClientDocument.client_id
LEFT JOIN rbDocumentType ON ClientDocument.documentType_id = rbDocumentType.id
;
'''

    config_file = 'vistamed.cfg'

    config = dict(
        host='127.0.0.1',
        db='b15',
        user='b15user',
        passwd='b15pass',
    )

    if os.path.exists(config_file):
        user_config = RawConfigParser()
        user_config.read(config_file)

        for param in config.keys():
            config[param] = user_config.get('database', param)

    app = QtGui.QApplication(sys.argv)

    MainWindow = QtGui.QMainWindow()

    ui = Ui_MainWindow(**config)
    ui.setupUi(MainWindow)

    MainWindow.show()

    sys.exit(app.exec_())
